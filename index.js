import logger from 'loglevel'
import {startServer} from './src/server/start.js';
import rollupConfig from './rollup.config.js';
import * as rollup from 'rollup';

logger.setLevel('info')

startServer()

const watcher = rollup.watch(rollupConfig);


// This will make sure that bundles are properly closed after each run
watcher.on('event', event => {
	console.log(event);
  // if (event.result) {
  // 	event.result.close();
  // }
});
