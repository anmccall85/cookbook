import * as entities from '../domain/entities/index.js';

const apiDoc = {
  openapi: '3.0.3',
  info: {
    title: 'Cookbook API.',
    version: '3.0.3'
  },
  paths: {},
  components: {
    schemas: entities
  },
};

export default apiDoc;