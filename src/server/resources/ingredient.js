
import express from 'express';
import knex from 'knex';
import * as uuid from 'uuid';
import {dataSources as dataSourcesSymbol} from '../../domain/services.js';


const dependencies = [dataSourcesSymbol];
function builder(db) {
		
	/**
	 * 
	 * @param {express.Request} req
	 * @param {express.Response} req
	 * @returns 
	 */
	function create(req, res, next) {
		const modelData = req.body.data;
		modelData.id = uuid.v4();
		db.table('ingredient')
			// .timeout(1000, {cancel: true})
			.insert(modelData, ['id'])
			.catch(err => {
				res.status(500).json(err);
			})
			.then(ids => db.select().from('ingredient').where({id: modelData.id}))
			.then(data => res.status(200).json(data[0]));
	}

	// NOTE: We could also use a YAML string here.
	create.apiDoc = {
		summary: 'Returns worlds by name.',
		operationId: 'getWorlds',
		parameters: [
			{
				in: 'query',
				name: 'worldName',
				required: true,
				type: 'string'
			}
		],
		responses: {
			200: {
				description: 'A list of worlds that match the requested name.',
				schema: {
					type: 'array',
					items: {
						$ref: '#/definitions/World'
					}
				}
			},
			default: {
				description: 'An error occurred',
				schema: {
					additionalProperties: true
				}
			}
		}
	};

	
	function search(req, res, next) {
		db.select('*')
			.from('ingredient')
			.then(data => {
				res.status(200).json({data});
			});
	}

	// NOTE: We could also use a YAML string here.
	search.apiDoc = {
		summary: 'Returns worlds by name.',
		operationId: 'getWorlds',
		parameters: [
			{
				in: 'query',
				name: 'worldName',
				required: true,
				type: 'string'
			}
		],
		responses: {
			200: {
				description: 'A list of worlds that match the requested name.',
				schema: {
					type: 'array',
					items: {
						$ref: '#/definitions/World'
					}
				}
			},
			default: {
				description: 'An error occurred',
				schema: {
					additionalProperties: true
				}
			}
		}
	};

	const operations = {
		create,
		search
	};

	return {operations};
};
export {
	dependencies,
	builder
};