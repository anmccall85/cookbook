
import express from 'express';
import knex from 'knex';
import {dataSourcesSymbol} from '../../domain/services.js';


const dependencies = [dataSourcesSymbol];
function builder(dataSources) {
	const db = knex({
		client: 'sqlite3',
		useNullAsDefault: false,
		connection: {
			filename: "dev.sqlite3",
		}
	});
		
    /**
     * 
     * @param {express.Request} req
     * @param {express.Response} req
     * @returns 
     */
		function POST(req, res, next) {
			res.status(200).json(worldsService.getWorlds(req.query.worldName));
		}

    // NOTE: We could also use a YAML string here.
    POST.apiDoc = {
      summary: 'Returns worlds by name.',
      operationId: 'getWorlds',
      parameters: [
        {
          in: 'query',
          name: 'worldName',
          required: true,
          type: 'string'
        }
      ],
      responses: {
        200: {
          description: 'A list of worlds that match the requested name.',
          schema: {
            type: 'array',
            items: {
              $ref: '#/definitions/World'
            }
          }
        },
        default: {
          description: 'An error occurred',
          schema: {
            additionalProperties: true
          }
        }
      }
    };
		function GET(req, res, next) {
			res.status(200).json(worldsService.getWorlds(req.query.worldName));
		}

    // NOTE: We could also use a YAML string here.
    GET.apiDoc = {
      summary: 'Returns worlds by name.',
      operationId: 'getWorlds',
      parameters: [
        {
          in: 'query',
          name: 'worldName',
          required: true,
          type: 'string'
        }
      ],
      responses: {
        200: {
          description: 'A list of worlds that match the requested name.',
          schema: {
            type: 'array',
            items: {
              $ref: '#/definitions/World'
            }
          }
        },
        default: {
          description: 'An error occurred',
          schema: {
            additionalProperties: true
          }
        }
      }
    };

    const operations = {
      POST,
      GET
    };

    return operations;
  };
export {
	dependencies,
	builder
};