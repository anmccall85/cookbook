import {LitElement, html} from 'lit';

class UserInfo extends LitElement {
  render() {
    return html`
      <div>Hello from MyElement!</div>
    `;
  }
}

globalThis.customElements.define('cb-userinfo', UserInfo);

export default UserInfo;