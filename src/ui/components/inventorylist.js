import {LitElement, html} from 'lit';

class InventoryList extends LitElement {
	items;

	constructor() {
		super();
		this.items = [
			{name: 'Candy', quantity: {value: 5, unit: 'box'}},
			{name: 'Ice', quantity: {value: 1, unit: 'lb'}},
		];
	}

  render() {
    return html`${this.items.map(item => html`<span>${item.name}</span>`)}`;
  }
}

globalThis.customElements.define('cb-inventorylist', InventoryList);

export default InventoryList;