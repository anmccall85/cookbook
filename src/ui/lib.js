
class DOMModel {

	/** @type {(string|class|undefined|null)} */
	el;

	/** @type {Object.<string, any>} */
	props;

	/** @type {Array<DOMModel>} */
	children;

	/**
	 * 
	 * @param {(string|class<HTMLElement>|undefined|null)} el 
	 * @param {Object.<string, any>} props 
	 * @param {Array<DOMModel>} children 
	 */
	constructor(el, props, children){
		this.el = el;
		if(children === undefined && (!props || props.constructor != Object)) {
			children = props;
			props = undefined;
		}
		this.props = props ?? {};
		this.children = (Array.isArray(children) || children === undefined)
			? children
			: [children]
		this.children = this.children || [];
	}

	/** @returns {HTMLElement|Node} */
	generateElement() {
		/** @type {HTMLElement} */
		const el = typeof this.el === 'string'
			? globalThis.document.createElement(this.el)
			: new this.el();
		if(!(el instanceof globalThis.HTMLElement)){
			throw new Error("Supplied element is not a child of HTMLElement: "+this.el)
		}
		el.append(...this.children.map(child => {
			if(child instanceof DOMModel) return child.generateElement();
			if(typeof child ===  'string') return child;
			const err = Error('Unknown child');
			err.child = child;
			throw err;
		}));
		Object.entries(this.props).map(([k, v]) => el[k] = v);
		return el;
	}
}

const h = (el, props, children) => new DOMModel(
	el, 
	props, 
	children
);
const hInputNumber = (props) => h('input', Object.assign({type: 'number'}, props))
const hOption = (value, display) => h('option', {value}, display === undefined ? value : display)


class DOMModelRendererElement extends globalThis.HTMLElement {

	set data(value) {
		this.$data = value;
		this.renderDOMModel();
	}

	get data() {
		return this.$data;
	}

	constructor(){
		super();
		this.renderDOMModel();
	}
	renderDOMModel() {
		const domModels = this.generateDOMModel();
		const nodes = domModels.map(domModel => domModel.generateElement());
		this.replaceChildren();
		if(nodes.length){
			this.replaceChildren(...nodes);
		} else {
		}
	}
	/** @returns {Array<DOMModel>} */
	generateDOMModel() {
		return [];
	}
}


export {
	DOMModel,
	DOMModelRendererElement,
	h,
	hOption,
	hInputNumber
}

