import { DOMModelRendererElement, h, hOption, hInputNumber } from "./lib";


const props = Symbol('props');
class InventoryItem {
  static [props] = {

  }
  constructor({
    id= undefined,
    ingredient= undefined,
    quantityValue= 0,
    quantityUnit= 'count',
  }) {
    this.ingredient = ingredient;
    this.quantityValue = quantityValue ?? 0;
    this.quantityUnit = quantityUnit ?? 'count';
  }
}

class Ingredient {
  constructor({
    id= undefined,
    name= 0,
  }) {
    this.id = id;
    this.name = name;
  }
}


class CookbookInventoryItemElement extends DOMModelRendererElement
{
  /** @returns {InventoryItem} data */
  // get data() { return super.data; }

  /**
   * @param {InputEvent} e
   * @param {HTMLElement} e.target
   */
  oninput(e) {
    const data = this.data;
    data[e.target.getAttribute('name')] = e.target.value;
    this.data = data;
    console.log(this.data);
  }
  generateDOMModel() {
    if(!this.data) return [];
    return [
      h('span', {}, this.data.ingredient),
      hInputNumber({name: 'quantityValue', oninput: e => this.oninput(e), value: this.data.quantityValue}),
      h('select', {name: 'quantityUnit', onchange: e => this.oninput(e), value: this.data.quantityUnit}, [
        hOption('count'),
        hOption('lbs', 'Pounds'),
        hOption('l', 'Liters'),
      ]),
      h('button', {}, 'Remove'),
    ];
  }
}


class CookbookIngredientSearch extends DOMModelRendererElement
{

  constructor(){
    super();
  }
  /**
   * 
   * @param {InputEvent} e
   * @param {HTMLElement} e.target
   */
  onsearch(e) {
    fetch('/api/v1/ingredients')
      .then(response => response.json())
      .then(data => data.data.map(item => new Ingredient(item)))
      .then(results => this.data = {results});
  }
  generateDOMModel() {
    return [
      h('legend', 'Search for ingredients'),
      h('input', {name: 'search', type: 'search', oninput:this.onsearch.bind(this)}),
      h('ol', {}, this.data?.results?.map(ing => {
        return h('li', {onItemDelete: (evt) => this.onItemDelete(evt)}, ing.name)
      })),
    ];
  }
}


class CookbookAppElement extends DOMModelRendererElement
{
  constructor(){
    super();
    this.data = [
      new InventoryItem({ingredient: 'Ingredient 1'}),
      new InventoryItem({ingredient: 'Ingredient 2'}),
      new InventoryItem({ingredient: 'Ingredient 3'}),
    ];
  }
  generateDOMModel() {
    return [
      h(CookbookIngredientSearch, {}),
      h('ol', (this.data || []).map(
        item => h('li', {}, [h(CookbookInventoryItemElement, {data: item})]))
      )
    ];
  }
}

customElements.define('cookbook-app', CookbookAppElement);
customElements.define('cookbook-inventoryitem', CookbookInventoryItemElement);
customElements.define('cookbook-ingredientsearch', CookbookIngredientSearch);

document.getElementById('main').appendChild(new CookbookAppElement());

// document.getElementById('sub').appendChild(new CookbookInventoryItemElement());

// navigator.mediaDevices.getUserMedia({video: true})
// .then(function(stream) {
//   console.log(stream)
// })
// .catch(function(err) {
//   console.log(err);
// });