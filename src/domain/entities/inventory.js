export default {
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://example.com/product.schema.json",
  "title": "Inventory",
  "description": "A product from Acme's catalog",
  "type": "object",
  "properties": {
    "id": {
      "description": "The unique identifier for a product",
      "type": "string",
      "format": "uuid"
    },
    "name": {
      "description": "Name of the product",
      "type": "string"
    }
  },
  "required": [ "id", "name" ]
}