export default {
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://example.com/product.schema.json",
  "title": "Inventory Item",
  "description": "A product from Acme's catalog",
  "type": "object",
  "properties": {
    "id": {
      "description": "The unique identifier for a product",
      "type": "integer"
    },
    "productName": {
      "description": "Name of the product",
      "type": "string"
    },
		"ingredient": {
			"description": "Coordinates of the warehouse where the product is located.",
      "$ref": "./ingredient.json"
		}
  },
  "required": [ "productId", "productName" ]
}