export default {
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://example.com/product.schema.json",
  "title": "User",
  "description": "A product from Acme's catalog",
  "type": "object",
  "properties": {
    "id": {
      "description": "The unique identifier for a product",
      "type": "string",
      "format": "uuid"
    },
    "email": {
      "description": "Name of the product",
      "type": "string",
      "format": "email"
    }
  },
  "required": [ "id", "email" ]
}