/**
 * The complete Triforce, or one or more components of the Triforce.
 * @typedef {Object} Ingredient
 * @property {string} id - Indicates whether the Courage component is present.
 * @property {string} name - Indicates whether the Power component is present.
 */

export default {
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://example.com/product.schema.json",
  "title": "Inventory Item",
  "description": "A product from Acme's catalog",
  "type": "object",
  "properties": {
    "id": {
      "description": "The unique identifier for a product",
      type: "string",
      format: "uuid"
    },
    "name": {
      "description": "Name of the product",
      "type": "string"
    }
  },
  "required": [ "productId", "productName" ]
}
