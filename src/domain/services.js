
const dataSources = Symbol('dataSources');
const logger = Symbol('logger');

export {
	dataSources,
	logger,
};
