import knex from 'knex';
import * as uuid from 'uuid';
import url from 'url';


const db = knex({
  client: 'sqlite3',
  useNullAsDefault: false,
  connection: {
    filename: "dev.sqlite3",
  }
});
console.log('connection started')
db.table('inventoryItem')
  .insert({
    id: uuid.v4(),
    name: 'test'
  })
  .then(res => {
    console.log(res);
    console.log('something')
  });
