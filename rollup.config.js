import { nodeResolve } from '@rollup/plugin-node-resolve';
import vue from 'rollup-plugin-vue';
import replace from 'rollup-plugin-replace';

export default {
  input: 'src/ui/app.js',
  external: [
    // 'vue',
  ],
  output: {
    file: 'public/js/file.js',
    format: 'es'
  },
	plugins: [
    nodeResolve(),
    vue(),
    replace({
      'process.env.NODE_ENV': JSON.stringify( 'production' )
    })
  ]
};
