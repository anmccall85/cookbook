export function up(knex) {
		console.log('trying')
  	return Promise.all([
			knex.schema.createTable('ingredient', function (table) {
				table.string('id').primary();
				table.string('name');
				table.timestamps();
			}),
			knex.schema.createTable('inventoryItem', function (table) {
				table.string('id').primary();
				table.string('name');
				table.timestamps();
			}),
		]);
	};

export function down(knex) {
		console.log('trying')
	};
