import express from 'express';

/**
 * @param {express.Request} req
 * @returns {express.Request} request
 */
export function mockRequest(args) {
	return Object.assign({

	}, args);
};

/**
 * @param {Function} arg.status
 * @returns {express.Response} request
 */
export function mockResponse(args) {
	const promises = {};
	const promisesResolves = {};
	if(args.status) {
		promises.status = new Promise(res => {
			promisesResolves.status = res;
		})
	}
	if(args.json) {
		promises.json = new Promise(res => {
			promisesResolves.json = res;
		})
	}
	let out = Object.assign({
		promises,
		status(status){
			if(args.status){
				promisesResolves.status(args.status(status));
			}
			return out;
		},
		json(json){
			if(args.json){
				promisesResolves.json(args.json(json));
			}
			return out;
		},
		then(func) {
			console.log('then', out.promises);
			return Promise.all(Object.values(out.promises)).then(ret => func(ret));
		}
	});
	return out;
};