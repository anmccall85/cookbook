import { JSDOM } from 'jsdom';
const { window } = new JSDOM(`...`);
globalThis.HTMLElement = window.HTMLElement;
globalThis.customElements = window.customElements;
globalThis.document = window.document;
