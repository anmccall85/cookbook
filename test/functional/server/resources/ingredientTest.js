import * as ingredient from '../../../../src/server/resources/ingredient.js';
import {mockRequest, mockResponse} from '../../../fixture/lib/express.js';
import knex from 'knex';
import * as sinon from 'sinon';
import {assert} from 'chai';

describe('server/reasources/ingredient#builder', () => {

	const db = knex({
		client: 'sqlite3',
		useNullAsDefault: false,
		connection: {
			filename: "dev.sqlite3",
		}
	});

	describe('#create', () => {
		it('should save submitted data', (done) => {
			const {operations} = ingredient.builder(db);
			const req = mockRequest({
				body: {
					data: {
						name: 'test'
					}
				}
			});
			const res = mockResponse({
				status(status){assert.equal(status, 200);},
				json(data) {console.log(data);}
			});
			operations.create(req, res, () => {});
			res.then(() => done());
		});
	});

	describe('#search', () => {
		it('should return saved data', (done) => {
			const {operations} = ingredient.builder(db);
			const req = mockRequest({
				body: {
					data: {
						name: 'test'
					}
				}
			});
			const res = mockResponse({
				status(status){assert.equal(status, 200);},
				json(data) {console.log(data);}
			});
			operations.search(req, res, () => {});
			res.then(() => done());
		});
	});
});
