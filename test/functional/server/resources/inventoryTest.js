import * as inventory from '../../../../src/server/resources/inventory.js';

describe('DOMModel', () => {
	describe('#constructor', () => {
		it('should handle el only', () => {
			const controller = inventory.builder(undefined);
			controller.POST()
			const domModel = new DOMModel('p');
			assert.strict.equal(domModel.el, 'p');
		});
	});
});
