import { DOMModel, DOMModelRendererElement, h } from '../../../src/ui/lib.js';
import assert from 'assert';
import { diff, addedDiff, deletedDiff, updatedDiff, detailedDiff } from 'deep-object-diff';

describe('DOMModel', () => {
	describe('#constructor', () => {
		it('should handle el only', () => {
			const domModel = new DOMModel('p');
			assert.strict.equal(domModel.el, 'p');
		});
	});
});

describe('DOMModelRendererElement', () => {

	class ExampleDOMModelRendererElement extends DOMModelRendererElement {		
		constructor() {
			super();
			this.data = {results: [
				{name: "Carrot"},
				{name: "Cake"}
			]};
		}
		onsearch(e) {
		}
		generateDOMModel() {
			return [
				h('legend', 'Search for ingredients'),
				h('input', {name: 'search', type: 'search', oninput:this.onsearch}),
				h('ol', {}, this.data?.results?.map(ing => {
					return h('li', {onItemDelete: (evt) => this.onItemDelete(evt)}, ing.name)
				})),
			];
		}
	};
	customElements.define('test-example', ExampleDOMModelRendererElement);

	describe('#generateDOMModel', () => {
		it('should handle el only', () => {
			const component = new ExampleDOMModelRendererElement();
			const domModel1 = component.generateDOMModel();
			component.data = {results: []};
			const domModel2 = component.generateDOMModel();
			console.log(diff(domModel1, domModel2));
		});
	});
});